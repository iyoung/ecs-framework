#pragma once
#include "Component.h"
#include <glm\gtx\quaternion.hpp>
#include <glm\glm.hpp>
class Orientation :
	public IComponent
{
public:
	Orientation();
	void setOrientation(const glm::vec3 &axis, const float &angleRadians);
	void setOrientation(const glm::quat &iOrientation);
	const glm::quat &getOrientation(){ return m_orientation; }
	virtual void setOwner(std::shared_ptr<Entity> owner){ m_owner = owner; }
	virtual ~Orientation();
private:
	glm::quat m_orientation;
	std::shared_ptr<Entity> m_owner;
};

