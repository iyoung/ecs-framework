#pragma once
#include "Message.h"
class TimerMessage :
	public IMessage
{
public:
	TimerMessage(const float &timeStepS, const int id);
	virtual const int getMessageID(){ return ID; }
	const float getTimeStep(){ return timeStep; }
	virtual ~TimerMessage();
private:
	float timeStep;
	int ID;
};

