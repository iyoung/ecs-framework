#pragma once
#include "INode.h"
class TestNode :
	public INode
{
public:
	TestNode(std::shared_ptr<Entity> entity);
	virtual unsigned int getID(){ return ID; }
	virtual ~TestNode();
private:
	unsigned int ID;
};

