#include "ECSTest.h"
#include "Engine.h"
#include "Entity.h"
#include "TestComponent.h"
#include "TestSystem.h"

ECSTest::ECSTest()
{
}
void ECSTest::runTest()
{
	std::shared_ptr<Engine> engine = std::make_shared<Engine>();
	std::shared_ptr<TestSystem> system = std::make_shared<TestSystem>( engine );
	engine->setFixedTimeStep( 1.0f / 60.0f );
	engine->addSystem( system );
	std::shared_ptr<IComponent> c = std::make_shared<TestComponent>();
	std::shared_ptr<Entity> e = std::make_shared<Entity>();
	e->addComponent( c );
	engine->addEntity( e );

	engine->startUp();
}

ECSTest::~ECSTest()
{
}
