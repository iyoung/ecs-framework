#pragma once
#include "Component.h"
class TestComponent :
	public IComponent
{
public:
	TestComponent();
	virtual ComponentType componentType() { return TEST_COMPONENT; }
	virtual void setOwner(std::shared_ptr<Entity> owner){ m_owner = owner; }
	virtual ~TestComponent();
private:
	std::shared_ptr<Entity> m_owner;
};

