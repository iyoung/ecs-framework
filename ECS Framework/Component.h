#pragma once
#include <memory>
//this enumerator decribes the basic types of components
enum ComponentType
{
	TEST_COMPONENT,
	NAME,
	POSITION,
	ORIENTATION,
	ROTATION,
	MOTION,
	TRANSFORM,
	STATS,
	SCORE,
	UIELEMENT,
	APPEARANCE,
	HEALTH,
	INVENTORY,
	WEAPON,
	PROJECTILE,
	EXPLOSION,
	OWNER,
	BODY,
	INPUT,
	STATE_LIFE,
	ANIMATION,
	BUFF,
	DEBUFF,
	AI_AGENT,
	NUM_COMPONENT_TYPES
};
//forward declaration of Entity type, to prevent circular inclusion errors
class Entity;
//Interface for component system. All components will implement this
class IComponent
{
public:
	IComponent();
	virtual ComponentType componentType() = 0;
	virtual void setOwner(std::shared_ptr<Entity> owner) = 0;
	virtual ~IComponent();
};

