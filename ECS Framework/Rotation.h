#pragma once
#include "Component.h"
#include <glm\gtx\quaternion.hpp>
#include <glm\glm.hpp>
class Rotation :
	public IComponent
{
public:
	Rotation();
	void setRotation(const glm::quat &iRotation);
	void setRotation(const glm::vec3 &axis, const float &angleRadiansPerSecond);
	void setAngularAcceleration(const glm::quat &iAcceleration);
	void setAngularAcceleration(const glm::vec3 &axis, const float &angleRadiansPerSecond);
	const glm::quat &getRotation(){ return rotation; }
	void stopRotation(){ ; }
	virtual ~Rotation();
private:
	glm::quat rotation;
};

