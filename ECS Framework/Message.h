#pragma once
#include "MessageID.h"
//interface for engine messages.
//Systems will create concrete messages to communicate with other systems
class IMessage
{
public:
	IMessage();
	virtual const int getMessageID() = 0;
	virtual ~IMessage();
};

