#pragma once
#include "Component.h"
#include <vector>
#include <ostream>
#include "HashFunction.h"

//base entity class. ideally this should should only store data
class Entity
{
public:
	Entity();
	//unique identifier for each entity
	unsigned int getUniqueID(){ return ID; }
	void setUniqueID(const unsigned int uniqueID){ ID = uniqueID; }
	bool addComponent(std::shared_ptr<IComponent> component);
	bool removeComponent(ComponentType componentID);
	bool hasComponent(ComponentType componentID);
	std::shared_ptr<IComponent> getComponent(ComponentType componentID);
	~Entity();
private:
	std::vector<std::shared_ptr<IComponent>> components;
	unsigned int ID;
};


