#pragma once
#include "Component.h"
//Component for physics systems:
//Stores relevant data for physics calculations
//inverse mass is calculated internally from mass, from 1/mass. inernal calculation makes sure mass is non-zero, to prevent divide by zero errors;
//density can be calculated and set manually, or as a function of the volume/area of the assigned convex shape/hull
//static & kinetic friction is used for sliding/traction/grip calculations, and models how much kinetic energy is lost to heat from a moving body contacting this one.
//restitution is how "bouncy" something is, ie. how much energy from collisions is converted to heat


//TODO: Finish this class
class Shape;

class Body :
	public IComponent
{
public:
	Body();
	virtual ComponentType componentType(){ return BODY; }
	//mutators
	virtual void setOwner(std::shared_ptr<Entity> owner){ m_owner = owner; }
	void setMass(const float &iMass){ ; }
	void setDensity(const float &iDensity){ ; }
	void setStaticFriction(const float &iStaticFriction){ ; }
	void setKineticFriction(const float &iKineticFriction){ ; }
	void setRestitution(const float &iRestitution){ ; }
	//accessors
	const float &getMass(){ return m_mass; }
	const float &getInverseMass(){ return m_inverseMass; }
	const float &getStaticFriction(){ return m_staticFriction; }
	const float &getKineticFriction(){ return m_kineticFriction; }
	const float &getRestitution(){ return m_Restitution; }
	virtual ~Body();
private:
	std::shared_ptr<Entity> m_owner;
	float m_mass,
		m_inverseMass,
		m_staticFriction,
		m_kineticFriction,
		m_Restitution,
		m_density; //kg per m^3/m^2
	std::shared_ptr<Shape> m_shape; //convex hull or 2d shape. General types are:
									// Rectangle, Cuboid, Circle, Sphere, ray(line), point
};

