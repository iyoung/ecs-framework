#include "RotationSystem.h"
#include "TimerMessage.h"

RotationSystem::RotationSystem(std::shared_ptr<Engine> engine) :ISystem(engine)
{
	active = false;
	my_engine = engine;
}
void RotationSystem::start()
{
	active = true;
	//my_nodes.resize(MAX_NODES, nullptr);
}
bool RotationSystem::addEntity(const std::shared_ptr<Entity> &entity)
{
	//if entity is viable, create a new unique instance of Rotation Node and place it into the appropriate index of the list
	if (entity->hasComponent(ORIENTATION) && entity->hasComponent(ROTATION))
	{
		my_nodes[entity->getUniqueID()].reset(new RotationNode(entity));
		return true;
	}
	return false;
}
bool RotationSystem::removeEntity(unsigned int uniqueID)
{
	//removes proxy node in constant time
	if (my_nodes[uniqueID].get() != nullptr)
	{
		delete my_nodes[uniqueID].release();
		return true;
	}
	return false;
}
void RotationSystem::handleMessage(std::shared_ptr<IMessage> message, Engine& engine)
{
	if (message->getMessageID() == MessageID::SystemShutdown)
	{
		shutDown();
	}
	else if (message->getMessageID() == MessageID::SystemStart)
	{
		start();
	}
	else if (message->getMessageID() == MessageID::SystemDeltaTime)
	{
		auto msg = std::dynamic_pointer_cast<TimerMessage>(message);
		update(msg->getTimeStep());
	}
}
void RotationSystem::update(const float &deltaTimeSeconds)
{
	if (active)
	{
		//walk the vector of nodes, and update each one if it exists.
		auto current = my_nodes.begin();
		auto end = my_nodes.end();

		while (current != end)
		{
			if ((*current) != nullptr) (*current)->update(deltaTimeSeconds);
		}
	}
}
void RotationSystem::shutDown()
{
	//clear the list of nodes set state
	//walk the vector of nodes, and clear pointers
	auto current = my_nodes.begin();
	auto end = my_nodes.end();

	while (current != end)
	{
		if ((*current) != nullptr) delete current->release();
	}
	active = false;
}
RotationSystem::~RotationSystem()
{
}
