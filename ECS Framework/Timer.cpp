#include "Timer.h"
#include "TimerMessage.h"
#include <time.h>
#include "Engine.h"
//---------------------------------------------------------------
//Engine Timer member functions
EngineTimer::EngineTimer()
{
	fixedTimeStepS = 0.0f;
	deltaTimeS = 0.0f;
	currentTimeAccumulatorS = 0.0;
	lastTimeS = 0.0f;
}
void EngineTimer::init()
{
	float currentTimeS = (float)clock() / CLOCKS_PER_SEC;
}
void EngineTimer::update(Engine &engine)
{
	float currentTimeS = (float)clock() / CLOCKS_PER_SEC;
	deltaTimeS = currentTimeS - lastTimeS;
	if (deltaTimeS >= fixedTimeStepS)
	{
		//printf("sending fixed time: %f\n",deltaTimeS);
		std::shared_ptr<IMessage> msg = std::make_shared<TimerMessage>(deltaTimeS, MessageID::SystemFixedTime);
		engine.handleMessage(msg);
	}
	else
	{
		//printf("sending delta time: %f\n", deltaTimeS);
		std::shared_ptr<IMessage> msg = std::make_shared<TimerMessage>(deltaTimeS, MessageID::SystemDeltaTime);
		engine.handleMessage(msg);
	}
	lastTimeS = currentTimeS;
	currentTimeAccumulatorS += deltaTimeS;
}

EngineTimer::~EngineTimer()
{
	fixedTimeStepS = 0.0f;
	deltaTimeS = 0.0f;
	currentTimeAccumulatorS = 0.0;
}