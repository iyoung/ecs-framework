#pragma once
#include "ISystem.h"
//interface for broadphase system for physics/collision detection
//this extends the ISystem interface, and generates pairs to be processed by
//a rigid/soft body physics system
//examples of concrete implementations are:

//Sweep and Prune (SAP)
//which maintains arrays of min/max extents of bodies, sort on each axis
//Quad/Octree
//which maintains trees of objects which are spacially seperated into cells

class IBroadphase :
	public ISystem
{
public:
	IBroadphase(std::shared_ptr<Engine> &engine);
	virtual void start() = 0;
	virtual void shutDown() = 0;
	virtual bool addEntity(std::shared_ptr<Entity> entity) = 0;
	virtual bool removeEntity(unsigned int uniqueID) = 0;
	virtual void handleMessage(const IMessage &message, Engine& engine) = 0;
	virtual void generatePairs() = 0;
	virtual ~IBroadphase();
};

