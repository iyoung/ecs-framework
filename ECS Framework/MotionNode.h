#pragma once
#include "INode.h"
#include "Position.h"
#include "Motion.h"
#include "Orientation.h"
#include "Rotation.h"
//proxy class, providing a layer of abstraction between Motion system and Entity/components.
//necessary for safe multi threading
class MotionNode :
	public INode
{
public:
	MotionNode(const std::shared_ptr<Entity> &entity);
	void update(const float &deltaTimeSeconds);
	virtual unsigned int getID(){ return ID; }
	virtual ~MotionNode();
private:
	std::shared_ptr<Position> position;
	std::shared_ptr<Motion> velocity;
	unsigned int ID;
};

