#pragma once
#include "ISystem.h"
#include <vector>
#include "TestNode.h"
class TestSystem :
	public ISystem
{
public:
	TestSystem(std::shared_ptr<Engine> engine);
	virtual void start();
	virtual void shutDown();
	virtual bool addEntity(const std::shared_ptr<Entity> &entity);
	virtual bool removeEntity(unsigned int uniqueID);
	virtual void handleMessage(std::shared_ptr<IMessage> message, Engine& engine);
	virtual ~TestSystem();
private:
	std::vector<std::shared_ptr<TestNode>> nodes;
	std::shared_ptr<Engine> m_engine;
	float timeaccumulator;
};

