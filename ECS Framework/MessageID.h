#pragma once
class MessageID
{
public:
	static const int TestMessage = 0;
	static const int SystemStart = 1;
	static const int SystemShutdown = 2;
	static const int SystemDeltaTime = 3;
	static const int SystemFixedTime = 4;
};

