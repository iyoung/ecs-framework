#pragma once
#include "INode.h"
#include "Rotation.h"
#include "Orientation.h"
class RotationNode :
	public INode
{
public:
	RotationNode(const std::shared_ptr<Entity> &entity);
	virtual unsigned int getID(){ return my_id; }
	void update(const float &deltaTimeSeconds);
	virtual ~RotationNode();
private:
	std::shared_ptr<Rotation> my_rotation;
	std::shared_ptr<Orientation> my_orientation;
	unsigned int my_id;
};

