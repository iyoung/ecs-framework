#pragma once
#include "ISystem.h"
#include "MotionNode.h"
#include <vector>
#include <array>
//System for dealing with linear and angular motion nodes.
//adding an entity creates a motion node which creates an abstraction layer (proxy) between the system and the entity
class MotionSystem :
	public ISystem
{
public:
	MotionSystem(std::shared_ptr<Engine> engine);
	virtual void start();
	virtual void shutDown();
	virtual bool addEntity(const std::shared_ptr<Entity> &entity);
	virtual bool removeEntity(unsigned int uniqueID);
	virtual void handleMessage(std::shared_ptr<IMessage> &message, Engine& engine);
	virtual ~MotionSystem();
private:
	void update(const float &deltaTimeSeconds);
	static const size_t MAX_NODES = 1024;
	std::array<std::unique_ptr<MotionNode>,MAX_NODES> my_nodes;
	bool active;
	std::shared_ptr<Engine> my_engine;
};

