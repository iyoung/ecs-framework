#pragma once
#include "Entity.h"
class EntityFactory
{
public:
	EntityFactory();
	//creates an empty entity
	static std::shared_ptr<Entity> createEntity();
	//creates an entity with components, loaded from a file
	static std::shared_ptr<Entity> createBasicEntity(std::string EntityFile);
	~EntityFactory();
};

