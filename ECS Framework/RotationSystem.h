#pragma once
#include "ISystem.h"
#include "RotationNode.h"
#include <array>
class RotationSystem :
	public ISystem
{
public:
	RotationSystem(std::shared_ptr<Engine> engine);
	virtual void start();
	virtual void shutDown();
	virtual bool addEntity(const std::shared_ptr<Entity> &entity);
	virtual bool removeEntity(unsigned int uniqueID);
	virtual void handleMessage(std::shared_ptr<IMessage> message, Engine& engine);
	virtual ~RotationSystem();
private:
	void update(const float &deltaTimeSeconds);
	std::shared_ptr<Engine> my_engine;
	static const size_t MAX_NODES = 1024;
	std::array<std::unique_ptr<RotationNode>,MAX_NODES> my_nodes;
	bool active;
};

