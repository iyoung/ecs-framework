#include "TestNode.h"


TestNode::TestNode(std::shared_ptr<Entity> entity) :INode(entity)
{
	printf("creating test node\n");
	ID = entity->getUniqueID();
}


TestNode::~TestNode()
{
	printf("destroying test node\n");
}
