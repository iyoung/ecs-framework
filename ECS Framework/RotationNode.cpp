#include "RotationNode.h"
#include <glm\gtx\quaternion.hpp>
#include <glm\gtc\quaternion.hpp>

RotationNode::RotationNode(const std::shared_ptr<Entity> &entity) :INode(entity)
{
	if (entity->hasComponent(ORIENTATION) && entity->hasComponent(ROTATION))
	{
		my_rotation = std::dynamic_pointer_cast<Rotation>(entity->getComponent(ROTATION));
		my_orientation = std::dynamic_pointer_cast<Orientation>(entity->getComponent(ORIENTATION));
		my_id = entity->getUniqueID();
	}
}

void RotationNode::update(const float &deltaTimeSeconds)
{
	//calculate the full rotated orientation based on rotations per second
	glm::quat rotation = my_rotation->getRotation() * my_orientation->getOrientation();
	//now do a spherical interpolation between oldorientation and new orientation, based on time since last frame
	my_orientation->setOrientation(glm::slerp(my_orientation->getOrientation(), rotation, deltaTimeSeconds));
}
RotationNode::~RotationNode()
{
}
