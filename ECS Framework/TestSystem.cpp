#include "TestSystem.h"
#include "TimerMessage.h"
#include "Engine.h"
#include "SystemMessage.h"

TestSystem::TestSystem(std::shared_ptr<Engine> engine) :ISystem(engine)
{
	timeaccumulator = 0.0f;
	printf("creating test system\n");
	m_engine = engine;
}

void TestSystem::start()
{
	printf("starting test system\n");
}
void TestSystem::shutDown()
{
	printf("shutting down test system\n");
	nodes.clear();
}
bool TestSystem::addEntity(const std::shared_ptr<Entity> &entity)
{
	printf("adding node to system\n");

	nodes.push_back(std::make_shared<TestNode>(entity));
	return true;
}
bool TestSystem::removeEntity(unsigned int uniqueID)
{
	printf("removing node from system\n");
	//searches for a node with ID in linear time, and removes it.
	std::vector<std::shared_ptr<TestNode>>::iterator current = nodes.begin();
	std::vector<std::shared_ptr<TestNode>>::iterator end = nodes.end();

	while (current != end)
	{
		if ((*current)->getID() == uniqueID)
		{
			current = nodes.erase(current);
			return true;
		}
		current++;
	}
	return false;
}
void TestSystem::handleMessage(std::shared_ptr<IMessage> message, Engine& engine)
{
	if (message->getMessageID() == MessageID::SystemShutdown)
	{
		shutDown();
		return;
	}
	if (message->getMessageID() == MessageID::SystemDeltaTime)
	{
		//printf("handling time step message\n");
		auto msg = std::dynamic_pointer_cast<TimerMessage>(message);
		timeaccumulator += msg->getTimeStep();
		printf("time accumulated: %f\n", (float)timeaccumulator);

	}
	if (message->getMessageID() == MessageID::TestMessage)
	{
		printf("handling test message\n");
	}
	if (message->getMessageID() == MessageID::SystemFixedTime)
	{
		//printf("handling fixed time step message\n");
		auto msg = std::dynamic_pointer_cast<TimerMessage>(message);
		timeaccumulator += msg->getTimeStep();
		printf("time accumulated: %f\n", (float)timeaccumulator);
	}

	if (timeaccumulator >= 5.0f)
	{
		printf("time limit reached.\n");
		std::shared_ptr<IMessage> msg = std::make_shared<SystemMessage>(MessageID::SystemShutdown);
		engine.handleMessage(msg);
	}
}
TestSystem::~TestSystem()
{
	printf("destroying test system\n");
}
