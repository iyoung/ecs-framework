#pragma once
#include "Message.h"
class SystemMessage :
	public IMessage
{
public:
	SystemMessage(const int &messageId);
	virtual const int getMessageID(){ return Id; }
	virtual ~SystemMessage();
private:
	unsigned int Id;
};

