#include "Engine.h"
#include <time.h>
#include "TimerMessage.h"
#include "Timer.h"
Engine::Engine()
{
	//this is nasty. can't use make_unique, it was left out of C++11. Still, no need to call delete
	timer = std::unique_ptr< EngineTimer >( new EngineTimer() );
}
void Engine::addSystem(std::shared_ptr< ISystem > system)
{
	systems.push_back(system);
	system->start();
}
void Engine::addEntity(std::shared_ptr< Entity > entity)
{
	std::vector< std::shared_ptr< ISystem > >::iterator system = systems.begin();
	std::vector< std::shared_ptr< ISystem > >::iterator end = systems.end();
	for (; system != end; system++)
	{
		(*system)->addEntity(entity);
		entities.push_back(entity);
	}
}
void Engine::startUp()
{
	running = true;
	while (running)
	{
		timer->update(*this);
	}
}
void Engine::shutDown()
{
	running = false;
	//std::vector< std::shared_ptr< ISystem > >::iterator system = systems.begin();
	//std::vector< std::shared_ptr< ISystem > >::iterator end = systems.end();
	//for (; system != end; system++)
	//{
	//	(*system)->shutDown();
	//}
	//systems.clear();
}
void Engine::setFixedTimeStep(const float &fixedTimeStepSeconds)
{
	timer->setFixedTimeStep(fixedTimeStepSeconds);
}
void Engine::handleMessage(std::shared_ptr<IMessage> &message)
{
	if (message->getMessageID() == MessageID::SystemShutdown)
	{
		shutDown();
	}
	else if (message->getMessageID() == MessageID::SystemStart)
	{
		startUp();
	}

	std::vector< std::shared_ptr< ISystem > >::iterator system = systems.begin();
	std::vector< std::shared_ptr< ISystem > >::iterator end = systems.end();
	for (; system != end; system++)
	{
		(*system)->handleMessage(message, *this);
	}

}
Engine::~Engine()
{
	systems.clear();
	entities.clear();
}


