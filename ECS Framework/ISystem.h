#pragma once
#include "Entity.h"
#include "Message.h"
class Engine;
//system interface for engine components
//all engine systems will implement this interface
class ISystem
{
public:
	ISystem(std::shared_ptr<Engine> engine);
	virtual void start() = 0;
	virtual void shutDown() = 0;
	virtual bool addEntity(const std::shared_ptr<Entity> &entity) = 0;
	virtual bool removeEntity(unsigned int uniqueID) = 0;
	virtual void handleMessage(std::shared_ptr<IMessage> message, Engine& engine) = 0;
	virtual ~ISystem();
};

