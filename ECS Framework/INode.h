#pragma once
#include "Entity.h"
//interface for system nodes
//this is implemented to create a layer between Entities and systems,
//allowing abstraction of system operations
class INode
{
public:
	INode(const std::shared_ptr<Entity> &entity);
	virtual unsigned int getID() = 0;
	virtual ~INode();
};

