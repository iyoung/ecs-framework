#pragma once
#include "Message.h"
class TestMessage :
	public IMessage
{
public:
	TestMessage();
	virtual const int getMessageID() { return MessageID::TestMessage; }
	virtual ~TestMessage();
};

