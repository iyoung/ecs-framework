#include "MotionNode.h"


MotionNode::MotionNode(const std::shared_ptr<Entity> &entity) :INode(entity)
{
	//find appropriate components within entity, and store a pointer to them within the node.
	if (entity->hasComponent(POSITION))
	{
		position = std::dynamic_pointer_cast<Position>(entity->getComponent(POSITION));
	}

	if (entity->hasComponent(MOTION))
	{
		velocity = std::dynamic_pointer_cast<Motion>(entity->getComponent(MOTION));
	}

	ID = entity->getUniqueID();
}

void MotionNode::update(const float &deltaTimeSeconds)
{
	//take velocity as m/s and apply it based on time since last update, in seconds
	if (position != nullptr && velocity != nullptr)
	{
		position->move(velocity->getVelocity() * deltaTimeSeconds);
	}
}
MotionNode::~MotionNode()
{
}
