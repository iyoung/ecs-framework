#include "MotionSystem.h"
#include "Engine.h"
#include "Message.h"
#include "Engine.h"
#include "TimerMessage.h"
MotionSystem::MotionSystem(std::shared_ptr<Engine> engine) :ISystem(engine)
{
	active = false;
	my_engine = engine;
}
void MotionSystem::handleMessage(std::shared_ptr<IMessage> &message, Engine& engine)
{
	if (message->getMessageID() == MessageID::SystemShutdown)
	{
		shutDown();
	}
	else if (message->getMessageID() == MessageID::SystemStart)
	{
		start();
	}
	else if (message->getMessageID() == MessageID::SystemDeltaTime)
	{
		auto msg = std::dynamic_pointer_cast<TimerMessage>(message);
		update(msg->getTimeStep());
	}

}
void MotionSystem::start()
{
	active = true;
}
void MotionSystem::shutDown()
{
	//clears all nodes, and set state
	auto current = my_nodes.begin();
	auto end = my_nodes.end();

	while (current != end)
	{
		if ((*current) != nullptr) delete current->release();
	}
	active = false;
}
bool MotionSystem::addEntity(const std::shared_ptr<Entity> &entity)
{
	//if entity is viable, create a new unique instance of Motion Node and place it into the appropriate index of the list
	if (entity->hasComponent(POSITION) && entity->hasComponent(MOTION))
	{
		my_nodes[entity->getUniqueID()].reset(new MotionNode(entity));
		return true;
	}
	return false;
}
bool MotionSystem::removeEntity(unsigned int uniqueID)
{
	//removes proxy node in constant time

	if (my_nodes[uniqueID].get() != nullptr)
	{
		delete my_nodes[uniqueID].release();
		return true;
	}
	return false;
}

void MotionSystem::update(const float &deltaTimeSeconds)
{
	if (active)
	{
		//walk the vector of nodes, updating them if they exist
		auto current = my_nodes.begin();
		auto end = my_nodes.end();

		while (current != end)
		{
			if ((*current) != nullptr) (*current)->update(deltaTimeSeconds);
		}
		current++;
	}
}
MotionSystem::~MotionSystem()
{
}
