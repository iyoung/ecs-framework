#include "Entity.h"


Entity::Entity()
{
	components.reserve(NUM_COMPONENT_TYPES);
}
bool Entity::addComponent(std::shared_ptr<IComponent> component)
{
	if (component->componentType() >= NUM_COMPONENT_TYPES || component->componentType() >= components.size())
		return false;
	component->setOwner(std::shared_ptr<Entity>(this));
	components[component->componentType()] = component;
	return true;
}
bool Entity::removeComponent(ComponentType componentID)
{
	if (!hasComponent(componentID))
		return false;
	components[componentID]->setOwner(nullptr);
	components[componentID] = nullptr;
	return true;
}
std::shared_ptr<IComponent> Entity::getComponent(ComponentType componentID)
{
	if (hasComponent(componentID))
	{
		return components[componentID];
	}
	return nullptr;
}
bool Entity::hasComponent(ComponentType componentID)
{
	if (componentID >= NUM_COMPONENT_TYPES || componentID >= components.size() || components[componentID] == nullptr)
		return false;
	return true;
}
Entity::~Entity()
{
	components.clear();
}
