#pragma once
#include <memory>

class Engine;
class EngineTimer
{
public:
	EngineTimer();
	void init();
	void update(Engine &engine);
	void setFixedTimeStep(const float &fixedTimeStepSeconds){ this->fixedTimeStepS = fixedTimeStepSeconds; }
	~EngineTimer();
private:
	float deltaTimeS;
	float fixedTimeStepS;
	float currentTimeAccumulatorS;
	float lastTimeS;
};
