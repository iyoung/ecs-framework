#pragma once
#include <memory>
class Entity;
class HashFunction
{
public:
	HashFunction();
	virtual unsigned int generateHashValue(std::shared_ptr<Entity> entity)=0;
	virtual ~HashFunction();
};

