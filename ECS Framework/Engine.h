#pragma once
#include "ISystem.h"
#include "Entity.h"
#include "Message.h"
#include <memory>
#include "Timer.h"
//main system manager class
//USE: instantiate an engine, then instantiate systems, adding engine as construction param
//add system to engine
//set fixed time step duration
//start engine
//instantiate entities and add to engine
//shutdown engine

//no update is required. 
//Timer system will trigger system updates
class Controller;
class Engine
{
public:
	Engine();
	void startUp();
	void addSystem(std::shared_ptr< ISystem > system);
	void addEntity(std::shared_ptr< Entity > entity);
	void handleMessage(std::shared_ptr<IMessage> &message);
	void setFixedTimeStep(const float &fixedTimeStepSeconds);
	void shutDown();
	~Engine();
private:
	std::vector<std::shared_ptr<ISystem>> systems;
	std::vector<std::shared_ptr<Entity>> entities;
	std::unique_ptr<EngineTimer> timer;
	std::unique_ptr<Controller> controller;
	bool running;
};

