#pragma once
#include "Component.h"
#include <glm\glm.hpp>
class Position :
	public IComponent
{
public:
	Position();
	virtual ComponentType componentType(){ return POSITION; }
	virtual void setOwner(std::shared_ptr<Entity> owner);
	void move(glm::vec3 offset);
	void moveTo(glm::vec3 newPosition);
	glm::vec3 getPosition(){ return position; }
	virtual ~Position();
private:
	glm::vec3 position;
	std::shared_ptr<Entity> m_owner;
};

