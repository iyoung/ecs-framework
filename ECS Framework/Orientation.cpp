#include "Orientation.h"


Orientation::Orientation()
{
}
void Orientation::setOrientation(const glm::vec3 &axis, const float &angleRadians)
{
	m_orientation = glm::quat(angleRadians, axis);
}
void Orientation::setOrientation(const glm::quat &iOrientation)
{
	m_orientation = iOrientation;
}

Orientation::~Orientation()
{
}
