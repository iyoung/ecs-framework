#pragma once
#include "Component.h"
#include <glm\glm.hpp>
class Motion :
	public IComponent
{
public:
	Motion();
	virtual ComponentType componentType(){ return MOTION; }
	virtual void setOwner(std::shared_ptr<Entity> owner){ m_owner = owner; }
	void addVelocity(glm::vec3 acceleration){ velocity += acceleration; }
	void setVelocity(glm::vec3 newVelocity){ velocity = newVelocity; }
	void stop(){ velocity = glm::vec3(0.0f); }
	glm::vec3 getVelocity(){ return velocity; }
	virtual ~Motion();
private:
	std::shared_ptr<Entity> m_owner;
	glm::vec3 velocity;
};

