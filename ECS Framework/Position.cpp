#include "Position.h"


Position::Position()
{
}
void Position::setOwner(std::shared_ptr<Entity> owner)
{
	m_owner = owner;
}
void Position::move(glm::vec3 offset)
{
	position += offset;
}
void Position::moveTo(glm::vec3 newPosition)
{
	position = newPosition;
}
Position::~Position()
{
}
